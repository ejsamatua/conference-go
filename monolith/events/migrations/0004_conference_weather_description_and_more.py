# Generated by Django 4.0.3 on 2022-12-22 23:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_conference_temperature_alter_conference_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='conference',
            name='weather_description',
            field=models.CharField(max_length=200, null=True),
        ),
        migrations.AlterField(
            model_name='conference',
            name='description',
            field=models.TextField(),
        ),
    ]
